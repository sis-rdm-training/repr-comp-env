# Introduction to Jupyter

## Learning goals
- Learn how to install and launch JupyterLab (or the classic Jupyter Notebook)
- Get to know the look-and-feel of the JupyterLab UI
- Learn how to open a new notebook, text document and terminal
- Learn how to interact with the Jupyter notebook in cell and edit mode
- Change parameters and code in an existing notebook
- Save and export the notebook

## Introduction
Jupyter is the generic name for a project to develop web-based, open-source tools for interactive computing in many different programming languages. Originally a branch of IPython, Jupyter has become an independent project and since then gained huge popularity in many different research fields. According to Wikipedia, "Project Jupyter's name is a reference to the three core programming languages supported by Jupyter, which are ***Ju***lia, ***Pyt***hon and ***R***, and also a homage to Galileo's notebooks recording the discovery of the moons of Jupiter." 

## Prerequisites
- Clone / download the [workshop repository](https://gitlab.ethz.ch/sis-rdm-training/repr-comp-env/-/tree/RDM-SS-2022/) 
to your computer
- If you cloned the repository, you need to switch to the correct branch: `git switch RDM-SS-2022`
- Change to the Jupyter folder and create a conda environment based on the [environment.yml](https://gitlab.ethz.ch/sis-rdm-training/repr-comp-env/-/blob/RDM-SS-2022/jupyter/environment.yml) file
- Activate the environment and start JupyterLab: `jupyter-lab`

## JupyterLab User Interface
The classic Jupyter Notebook presented a very clean but rather feature-poor UI. JupyterLab on the other hand is becoming more and more a full-featured Integrated Development Environment (IDE), similar to MATLAB or RStudio. We will focus on JupyterLab in this workshop. However, you can still switch from Lab to Notebook using `Help --> Launch Classic Notebook`.

The JupyterLab UI consists of different sections. The main (largest) work area contains the running notebooks, consoles, text etc. When you first start JupyterLab, it simply shows you a "Launcher" to start new Notebooks, Consoles or other items. The way the "Launcher" looks depends on the user setup, but it will be similar to this screenshot:
 
![](images/jupyter-launcher.png)

## The File Browser
The File Browser shows the files in the current directory and allows navigating between different directories (relative to the starting directory). You can also upload new files or create new folders and perform some standard file operation (rename, delete etc.). 

## Launching new notebooks / text documents
To create a new notebook or text document, you can use either the Launcher or the `File --> New --> Notebook / Text File` menu. When you create a notebook from the menu, you will have to select a Kernel for the notebook. Just choose the default one: *Python 3*. Newly created documents are automatically saved - `Untitled.ipynb` for notebooks and `untitled.txt` for text files. Change the name to something more meaningful in the File Browser or by right-clicking on the notebook / file name at the top of the main work area.

## Launching new consoles
A console provides the same computational environment as a notebook (i.e. the same Kernel) but everything you type in a console is ephemeral - once you close it, it is gone. Thus, consoles are most useful for quickly trying out things. Start a new *Python 3* Console and insert the following Python code to generate a random number (to run the code hit `Shift+Enter`):

```{.python .number-lines}
import numpy as np

np.random.seed()
print("Your random number is: ", np.random.randint(0, 100))
```

## Launching new terminals
The Terminal provides access to the system shell of your computer (or the server on which JupyterLab is running). For Linux/Mac this is the configured system shell (bash, tsch etc.) and for Windows the PowerShell. To open a new Terminal, select `File --> New --> Terminal`. As an example, type the command `ls -lsah` (on Linux / Mac with bash) or `Get-ChildItem` (on Windows PowerShell) to get a list of the files / folders and their attributes in the current directory.

## Managing Tabs
You might have quite a few items running in your work area at a given time: Notebooks, Consoles, Output views etc. You can rearrange these different tabs simply by dragging them around the work area. Once you start using JupyterLab more extensively, you might want to save the layout of items or even keep different layouts for different projects. This is possible with the concept of *Workspaces* in JupyterLab.

## Working with Jupyter notebooks

## Notebook Basics
Run through [Notebook_Basics.ipynb](https://gitlab.ethz.ch/sis-rdm-training/repr-comp-env/-/blob/RDM-SS-2022/jupyter/Notebook_Basics.ipynb) to learn about basic interaction in Jupyter notebooks

## Working with a simple analysis notebook
Open [Basic_Image_Analysis_Notebook.ipynb](https://gitlab.ethz.ch/sis-rdm-training/repr-comp-env/-/blob/RDM-SS-2022/jupyter/Basic_Image_Analysis_Notebook.ipynb) and try to run the analysis notebook from top to bottom. Change a few parameters to see how the result changes. 

## Exporting the notebook
Save your notebook (`File -> Save`) and download it (`File -> Download`). 

Export a static HTML version of your notebook (`File -> Export Notebook as`). Also try a few other export options. Most likely you will get an error for some export options, as these require additional tools to be installed (like LaTeX).
