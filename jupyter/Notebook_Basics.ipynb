{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Basics\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This lesson assumes that the user has Jupyter [installed](https://jupyter.readthedocs.io/en/latest/install.html). JupyterLab can be started by running:\n",
    "`jupyter-lab`\n",
    "\n",
    "Depending on the installed version, the classic Jupyter notebook can be started by running one of these commands:\n",
    "\n",
    "`jupyter-nbclassic`\n",
    "\n",
    "or\n",
    "\n",
    "`jupyter notebook`\n",
    "\n",
    "Below we assume the new JupyterLab interface. The corresponding introduction to the classic notebook can be found [here](https://github.com/ipython/ipython-in-depth/blob/master/examples/Notebook/Notebook%20Basics.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Notebook\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When a notebook is opened, a new JupyterLab tab will be created which presents the notebook user interface (UI). This UI allows for interactively editing and running the notebook document.\n",
    "\n",
    "A new notebook can be created from the JupyterLab Launcher or by `File -> New -> Notebook`. To change the name of the notebook, right-click on the name in the File Browser and select `Rename`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Header\n",
    "\n",
    "At the top of the notebook document is a menubar to perform a variety of actions which control notebook navigation and document structure:\n",
    "\n",
    "<img src=\"images/menubar-notebook.png\" />\n",
    "\n",
    "The **Notebook Kernel** is indicated in the right part of the menubar (*Python 3* in the image above). The kernel determines the programming language of the notebook as well as the available packages / modules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Body\n",
    "\n",
    "The body of a notebook is composed of cells. Each cell contains either markdown, code input, code output, or raw text. Cells can be included in any order and edited at-will, allowing for a large ammount of flexibility for constructing a narrative.\n",
    "\n",
    "- **Markdown cells** - These are used to build a nicely formatted narrative around the code in the document. The majority of this lesson is composed of markdown cells.\n",
    "\n",
    "- **Code cells** - These are used to define the computational code in the document. They come in two forms: the *input cell* where the user types the code to be executed, and the *output cell* which is the representation of the executed code. Depending on the code, this representation may be a simple scalar value, or something more complex like a plot or an interactive widget.\n",
    "\n",
    "- **Raw cells** - These are used when text needs to be included in raw form, without execution or transformation.\n",
    "\n",
    "<img src=\"images/notebook_body_4_0.png\" />"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Modality\n",
    "\n",
    "The notebook user interface is *modal*. This means that the keyboard behaves differently depending upon the current mode of the notebook. A notebook has two modes: **edit** and **command**. The current mode is indicated in the status bar at the bottom of the notebook.\n",
    "\n",
    "When a cell is in **edit mode**, you can type into the cell, like a normal text editor.\n",
    "\n",
    "When in **command mode**, the structure of the notebook can be modified as a whole, but the text in individual cells cannot be changed. Most importantly, the keyboard is mapped to a set of shortcuts for efficiently performing notebook and cell actions. For example, pressing **`c`** when in command mode, will copy the current cell; no modifier is needed.\n",
    "\n",
    "<br>\n",
    "<div class=\"alert alert-success\">\n",
    "Enter edit mode by pressing `Enter` or using the mouse to double-click on a cell's editor area.\n",
    "</div>\n",
    "<div class=\"alert alert-success\">\n",
    "Enter command mode by pressing `Esc` or using the mouse to click *outside* a cell's editor area.\n",
    "</div>\n",
    "<div class=\"alert alert-warning\">\n",
    "Do not attempt to type into a cell when in command mode; unexpected things will happen!\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Mouse navigation\n",
    "\n",
    "The first concept to understand in mouse-based navigation is that **cells can be selected by clicking on them.** The currently selected cell is indicated with a blue bar. Clicking inside a cell's editor area will enter edit mode. Clicking on the prompt or the output area of a cell will enter command mode.\n",
    "\n",
    "The second concept to understand in mouse-based navigation is that **cell actions usually apply to the currently selected cell**. For example, to run the code in a cell, select it and then click the <button class='btn btn-default btn-xs'><i class=\"fa fa-play icon-play\"></i></button> button in the toolbar or the **`Run -> Run selected cells`** menu item. Similarly, to copy a cell, select it and then click the <button class='btn btn-default btn-xs'><i class=\"fa fa-copy icon-copy\"></i></button> button in the toolbar or the **`Edit -> Copy cells`** menu item. With this simple pattern, it should be possible to perform nearly every action with the mouse.\n",
    "\n",
    "Markdown cells have one other state which can be modified with the mouse. These cells can either be rendered or unrendered. When they are rendered, a nice formatted representation of the cell's contents will be presented. When they are unrendered, the raw text source of the cell will be presented. To render the selected cell with the mouse, click the <button class='btn btn-default btn-xs'><i class=\"fa fa-play icon-play\"></i></button> button in the toolbar or the **`Run -> Run selected cells`** menu item. To unrender the selected cell, double click on the cell."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Keyboard Navigation\n",
    "\n",
    "The modal user interface of the Notebook has been optimized for efficient keyboard usage. This is made possible by having two different sets of keyboard shortcuts: one set that is active in edit mode and another in command mode.\n",
    "\n",
    "The most important keyboard shortcuts are **`Enter`**, which enters edit mode, and **`Esc`**, which enters command mode.\n",
    "\n",
    "In edit mode, most of the keyboard is dedicated to typing into the cell's editor. Thus, in edit mode there are relatively few shortcuts. In command mode, the entire keyboard is available for shortcuts, so there are many more possibilities.\n",
    "\n",
    "The following shortcuts have been found to be the most useful in day-to-day tasks:\n",
    "\n",
    "- Basic navigation: **`enter`**, **`shift-enter`**, **`up/k`**, **`down/j`**\n",
    "- Cell types: **`y`**, **`m`**, **`1-6`**, **`r`**\n",
    "- Cell creation: **`a`**, **`b`**\n",
    "- Cell editing: **`x`**, **`c`**, **`v`**, **`d`**\n",
    "- Kernel operations: **`i`**, **`0`** (press twice)\n",
    "\n",
    "All commands are also available from the JupyterLab menus, where the corresponding keyboard shortcuts are also conveniently indicated. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
