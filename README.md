# ETH RDM Summer School 2022
## Workshop 3 - Tools and Platforms for Reproducible Data Analysis

## Learning objectives
Students will learn:
- conceptual and practical aspects of computational reproducibility,
- how to use a package and environment manager such as Conda,
- about advantages and pitfalls of computational notebooks,
- about containerisation to define and distribute computational environments,
- how to use a reproducibility platform to seamlessly share computational work with others.


## Prerequisites
In this workshop, we will introduce several tools and platforms for reproducible scientific computing. For some tools, only a web browser is required. Others require installation of software on your own computer. For the hands-on sessions, it is recommended to install these tools beforehand. 
1. Conda
- In case you do not have it, please install Anaconda Python 3.9 64-bit version in advance https://docs.continuum.io/anaconda/install/ (it will take some time). Please verify your installation https://docs.continuum.io/anaconda/install/verify-install/ and feel comfortable starting the command line interface (Terminal, PowerShell). More advanced participants may also choose to install the Miniconda distribution instead (see https://docs.conda.io/en/latest/miniconda.html).

## [License](./LICENSE)

The entire content is distributed under Attribution-NonCommercial-ShareAlike 4.0 International ([CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)).
