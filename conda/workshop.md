# Introduction to Conda
## Reference

- [Getting Started](https://docs.conda.io/projects/conda/en/latest/user-guide/getting-started.html)
- [Managing environments](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)
- [Cheat Sheet](https://docs.conda.io/projects/conda/en/latest/_downloads/843d9e0198f2a193a3484886fa28163c/conda-cheatsheet.pdf)
- [Conda](https://docs.conda.io/en/latest/)

## Starting conda 

**Command Line Interface**

- Linux: open a terminal 
- macOS: press `command + Space` and search `Terminal`
- Windows: search `anaconda` and select Anaconda Prompt or Anaconda Powershell Prompt

```{.bash .number-lines}
$ conda deactivate
```

:::::::::::::: {.columns}
::: {.column width="50%"}
![Conda on Windows](images/conda_windows.png){ width=60% }  
:::

::: {.column width="50%"}
![Conda on macOS](images/conda_macos.png){ width=60% }
:::
::::::::::::::


## Version, Info and Help

conda Environment

: A directory that contains specific installed packages (system libraries, python/R modules, ...). 

base Environment

: The default environment that you have when installing conda. 

```{.bash .number-lines}
$ conda --version

$ conda --help

$ conda info
```
. . .

### Exercise

::: nonincremental

1. Which version of conda are you running?

2. Inspect the output of `conda info` especially: platform, Python version, active environment, base environment and channel URLs.

:::

## Create / Clone and List all Envs 

```{.bash .number-lines}
$ conda create --name py38 python=3.8

$ conda create --name py38-clone --clone py38

$ conda env list
```

- in `--name py38` - **py38** is the name of the environment
- in `python=3.8` - **python** package version **3.8** is installed in the new environment
- in `--clone py38` - **py38** is the name of the cloned existing environment
- `conda env list` - is equivalent with `conda info --env`

## Activate and Deactivate Env

Check Python version on:

1. the system,
2. the **base** env, and 
3. the new **py38** env.

```{.bash .number-lines}
      $ python --version

      $ conda activate 
(base)$ python --version

(base)$ conda activate py38
(py38)$ python --version

(py38)$ conda deactivate --help
```

- in `conda activate` - the default (**base**) environment is activated
- in `conda activate py38` - **py38** is the name of the environment

## List Installed Packages

```{.bash .number-lines}
      $ conda activate py38
(py38)$ conda list
(py38)$ conda list --name base

(py38)$ conda list python
(py38)$ conda list --name base python
```

- in `list --name base` - **base** is the name of the environment
- in `list python` - only packages matching **python** string (regular expression) in the active environment (or base if no one active)

## Search and Install Packages

Conda channels

: Locations where packages are stored, e.g. conda-forge, bioconda, r;

- channels have priority (left to right)

- search using the browser [anaconda.org](https://anaconda.org/)

```{.bash .number-lines}
(py38)$ conda deactivate && conda deactivate
      $ conda search numpy=1.20
      $ conda search --channel conda-forge numpy=1.20

      $ conda install --channel conda-forge numpy=1.20.3 --name py38
      $ conda activate py38
(py38)$ conda install pandas="<1"
```

- in `install --channel conda-forge ...` - `conda-forge` is used additionally, with higher priority, to the list of channels
- in `install pandas="<1"` - the quotes are mandatory 
- more about [Installing with conda](https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/installing-with-conda.html)


## Update Packages and Revisions

```{.bash .number-lines}
(py38)$ conda update --channel conda-forge pandas
(py38)$ conda update --all

(py38)$ conda list --revisions
(py38)$ conda install --revision 3 --channel conda-forge
(py38)$ conda list --revisions
```

- in `update ... pandas` - update **pandas** to the latest compatible version 
- in `update --all` - update all packages 
- in `list --revisions` - list history of changes
- in `install --revision 3` - restore to revision **3**

**Note**: In restoring to a revision the additional channels are needed explicitly.

## Remove Package / Env and Clean

```{.bash .number-lines}
(py38)$ conda env list
(py38)$ conda remove --name py38 pandas
(py38)$ conda remove --name py38-clone --all
(py38)$ conda env list

(py38)$ conda clean --all
```

- in `remove --name py38 pandas` - **pandas** package is removed
- in `conda remove --name py38-clone --all` - **py38-clone** environment is removed 
- `clean --all` - remove unused packages and caches

**Note**: It is not possible to remove the active environment.

. . . 

### Exercise

::: nonincremental

- List all revisions in `py38` environment. Do you recognize the changes in the last revision?

:::

## Environment File - YAML Format

Environment File

: A YAML file that contains all information to create an environment.

YAML file: `conda/mamba.yml`

```{.yaml .number-lines}
name: mamba
channels:
  - conda-forge
dependencies:
  - mamba
```

**Note**: **`mamba`** is like **`conda`** but faster.


. . .

- go to the parent folder of the yaml file

```{.bash .number-lines}
(py38)$ cd src
```


## Environment File to Environment and back

```{.bash .number-lines}
 (py38)$ conda env create --file mamba.yml
 (py38)$ conda activate mamba

(mamba)$ conda env export
(mamba)$ conda env export --no-builds
(mamba)$ conda env export --from-history
(mamba)$ conda env export --no-builds > mamba_export.yaml
(mamba)$ conda env create --file mamba_export.yaml
CondaValueError: prefix already exists: ...
(mamba)$ conda env create --file mamba_export.yaml --name mamba_export
```

- in `env export` - all installed packages are exported (in **yaml** format)
- in `env export --no-builds` - the build specification is removed
- in `env export --from-history` - all packages that you explicitly asked for
- platform information is not exported 

**Note**: The build specification is generally platform dependent.

**Note**: The packages might not be available on all platforms.


## Identical Environments - Spec File

Spec File

: A text file that contains all information to create an identical environment.

```{.bash .number-lines}
(mamba)$ conda list --explicit 

(mamba)$ conda list --explicit > mamba_spec_file.txt
(mamba)$ conda create --name mamba_explicit --file mamba_spec_file.txt
```

- in `conda create --file mamba_spec_file.txt` - conda does not check the platform nor the dependencies 
- platform information is exported 

**Warning**: Use the spec file on a similar platform. 

. . .

::: nonincremental

### Exercise

1. List all environments.
2. Read the help for `conda remove` and pay attention at the flag `-y, --yes` .
2. Remove `mamba_export` and `mamba_explicit` environments.

:::

## Conda Optional Arguments

There are long `--` and potentially equivalent short `-` optional arguments.

| long: `--`     	| short: `-` 	| Remark     	|
|---------------	|-----------	|------------	|
| `--name`      	| `-n`      	|            	|
| `--channel`   	| `-c`      	|            	|
| `--help`      	| `-h`      	|            	|
| `--file`      	| `-f`      	| not always 	|
| `--all`       	| `-a`      	| not always 	|
| `--revisions` 	| `-r`      	|            	|
| `--yes`     	| `-y`      	|            	|

**Note**: If an argument is not working as expected use `--help`.


## What Did We Learn?

- we can create new environments
- we can install and update packages, from desired channels and asking for a specific version
- once we are happy with the environment we can create an environment file that allows us to reproduce the desired environment 
- for long-term preservation we can export the environment in a new file and list explicitly the installed packages
