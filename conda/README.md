# Docker and Conda Hands-On

[Pandoc](https://pandoc.org/) can be used to generate both standalone html pages, and slides, using a markdown input file.

The documentation for defining the source format of slides is available [here](https://pandoc.org/MANUAL.html#slide-shows).

In practice the [Slidy](https://www.w3.org/Talks/Tools/Slidy2/) format was used.

## Requirements

- [Pandoc](https://pandoc.org/) (v2.16.2 used previously)

Optional:
- [course-tool](https://sissource.ethz.ch/sis/courses/sis-course-website) to upload the content on [https://siscourses.ethz.ch/](https://siscourses.ethz.ch/)

## Generate Slidy and HTML

In the terminal 

```bash
# Generate the slidy format
$ pandoc -t slidy -o Conda_and_Docker.slidy.html --defaults metadata.yaml

# Generate the html format
$ pandoc -t html5 -o Conda_and_Docker.html --defaults metadata.yaml --toc -V toc-title:"Table of Contents"
```

## Optional - Upload to https://siscourses.ethz.ch/ 

```bash
# Optionally you can upload the content to https://siscourses.ethz.ch/ 
$ course-tool upload conda_and_docker Conda_and_Docker.*
```
